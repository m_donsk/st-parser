<?php

namespace App\Http\Controllers;

use App\Support\PageParser;
use App\Support\QueryParser;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use GuzzleHttp\Exception\TransferException;

class ParseController extends Controller
{
    public function run(Request $request): Collection
    {
        $input = $request->input('input');
        abort_unless($input, 400, 'Empty request');

        $query = new QueryParser($input);

        $url = $query->getUrl();
        abort_unless($url, 400, 'URL not found');

        $identifiers = $query->getSelectors();
        abort_if($identifiers->isEmpty(), 400, 'There are no classes or ids');

        try {
            $parser = PageParser::fromUrl($url);
        } catch (TransferException $e) {
            abort(400, 'Invalid URL');
        }

        return $parser->parse($identifiers->implode(' '));
    }
}
