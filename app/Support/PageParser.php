<?php

namespace App\Support;

use HTMLPurifier;
use GuzzleHttp\Client;
use HTMLPurifier_Config;
use Illuminate\Support\Collection;
use Symfony\Component\DomCrawler\Crawler;

class PageParser
{
    protected $html;

    public function __construct(string $html)
    {
        $this->html = $html;
    }

    /**
     * @param string $url
     * @return PageParser
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function fromUrl(string $url): PageParser
    {
        $client = new Client(['base_uri' => $url, 'timeout' => 10]);

        $response = $client->request('get');

        return new self((string) $response->getBody());
    }

    public function parse(string $query): Collection
    {
        $elements = $this->getElements($query);

        return $this->purifyElements($elements);
    }

    protected function getElements(string $query): Collection
    {
        $crawler = new Crawler($this->html);

        return collect($crawler->filter($query)->each(function (Crawler $node) {
            return $node->html();
        }));
    }

    protected function purifyElements(Collection $elements): Collection
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', 'p,h1,h2,h3,h4,h5,h6,table,tbody,thead,tfoot,th,tr,td');

        $purifier = new HTMLPurifier($config);

        return $elements->map(function ($item) use ($purifier) {
                return trim($purifier->purify($item));
            })
            ->filter(function ($item) {
                return ! empty($item);
            });
    }
}
