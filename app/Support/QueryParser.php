<?php

namespace App\Support;

use Illuminate\Support\Collection;

class QueryParser
{
    protected $input;

    public function __construct(string $input)
    {
        $this->input = $input;
    }

    public function getUrl(): string
    {
        preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $this->input, $match);

        return count($match[0]) > 0 ? $match[0][0] : '';
    }

    public function getSelectors(): Collection
    {
        preg_match_all('#(class|id)="([^"]*)"#', $this->input, $match);

        return collect($match[0])->map(function ($item) {
            $type = starts_with($item, 'class') ? '.' : '#';

            preg_match('#^(class|id)="(.+)"$#', $item, $matches);

            // Join classes with a dot, because there could be several classes
            $name = $type === '.' ? str_replace(' ', '.', $matches[2]) : $matches[2];

            return $type.$name;
        });
    }
}
