<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Support\PageParser;

class PageParserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $html = file_get_contents(__DIR__.'/pages/typical-page.html');

        $parser = new PageParser($html);

        $elements = $parser->parse('#main_content .main_block_of_content .mboc_text');

        $this->assertArraySubset(['Test1', 'Test2', 'Test3'], $elements);
    }
}
