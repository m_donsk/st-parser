<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Support\QueryParser;

class QueryParserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTypicalQuery()
    {
        $query = file_get_contents(__DIR__.'/queries/typical-query');

        $parser = new QueryParser($query);

        $url = $parser->getUrl();
        $selectors = $parser->getSelectors();

        $this->assertEquals('https://www.sunrise-tour.ru/russia/chernoe-more/tury/', $url);
        $this->assertArraySubset(['#main_content', '.main_block_of_content', '.mboc_text'], $selectors);
    }
}
